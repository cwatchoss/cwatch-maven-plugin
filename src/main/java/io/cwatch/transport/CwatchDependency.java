/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.cwatch.transport;

import org.apache.maven.model.Dependency;

/**
 * CwatchDependency takes a Dependency data structure from maven
 * and copies it here.
 * <p>
 * Reason is to have a clear inteface between maven and to make sure that
 * only the wanted/needed data is copied and nothing else.
 *
 * @author Thorsten Zenker
 * @see CwatchBus
 */


public class CwatchDependency {

    String groupId;
    String artifactId;
    String version;
    String classifier;
    String scope;
    String optional;

    public CwatchDependency() {

    }


    public CwatchDependency GenCwatchDependency(Dependency d) {
        CwatchDependency cwd = new CwatchDependency();

        cwd.setGroupId(d.getGroupId());
        cwd.setArtifactId(d.getArtifactId());
        cwd.setVersion(d.getVersion());
        cwd.setClassifier(d.getClassifier());
        cwd.setScope(d.getScope());
        cwd.setOptional(d.getOptional());


        return cwd;
    }


    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public void setOptional(String optional) {
        this.optional = optional;
    }
}
