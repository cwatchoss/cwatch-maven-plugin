/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.transport;

import org.eclipse.aether.artifact.Artifact;


/**
 * Artifact used by cWatch
 * Very similar to the org.eclipse.aether.artifact.Artifact with information stripped.
 * Main reason: control data.
 *
 * @author Thorsten Zenker
 */


public class CwatchArtifact {

    String groupId;
    String artifactId;
    String version;
    String classifier;
    String extension;

    public CwatchArtifact() {

    }


    public CwatchArtifact GenCwatchArtifact(Artifact a) {

        CwatchArtifact cwa = new CwatchArtifact();

        cwa.setGroupId(a.getGroupId());
        cwa.setArtifactId(a.getArtifactId());
        cwa.setVersion(a.getVersion());
        cwa.setClassifier(a.getClassifier());
        cwa.setExtension(a.getExtension());

        return cwa;
    }


    public String getGroupId() {
        return groupId;
    }

    public void setGroupId(String groupId) {
        this.groupId = groupId;
    }

    public String getArtifactId() {
        return artifactId;
    }

    public void setArtifactId(String artifactId) {
        this.artifactId = artifactId;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }

    public String getClassifier() {
        return classifier;
    }

    public void setClassifier(String classifier) {
        this.classifier = classifier;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }
}
