/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.cwatch.transport;

import org.apache.maven.model.Dependency;
import org.eclipse.aether.artifact.Artifact;

import java.util.ArrayList;
import java.util.List;

/**
 * Data structure: what to put into the content iof the bus.
 *
 * @author Thorsten Zenker
 */

public class CwatchBusContent {

    // This goes into one JSon String and goes into CwatchBus.content

    List<CwatchDependency> pomDeps = new ArrayList<CwatchDependency>();             // deps from pom
    List<CwatchArtifact> allArts = new ArrayList<CwatchArtifact>();               // all artifacts involved
    List<CwatchArtifact> directArts = new ArrayList<CwatchArtifact>();            // direct deps
    List<CwatchArtifact> recursiveArts = new ArrayList<CwatchArtifact>();         // recursive deps
    String BowerJsonContent = null;                                  // historic .. not used anymore // optional bower.json file .. if user wants to


    public CwatchBusContent() {

        this.pomDeps = new ArrayList<CwatchDependency>();            // deps from pom
        this.allArts = new ArrayList<CwatchArtifact>();              // all artifacts involved
        this.directArts = new ArrayList<CwatchArtifact>();           // direct deps
        this.recursiveArts = new ArrayList<CwatchArtifact>();
        this.BowerJsonContent = "";
    }


    public void loadBus(List<Dependency> thisProjectDependencies, List<Artifact> dependencies,
                        List<Artifact> directDependencies,
                        List<Artifact> recursiveDependencies) {

        if (thisProjectDependencies != null) {
            for (Dependency d : thisProjectDependencies) {
                CwatchDependency cwd = new CwatchDependency().GenCwatchDependency(d);
                pomDeps.add(cwd);
            }
        }

        if (dependencies != null) {
            for (Artifact a : dependencies) {
                CwatchArtifact cwa = new CwatchArtifact().GenCwatchArtifact(a);
                allArts.add(cwa);
            }
        }

        if (directDependencies != null) {
            for (Artifact a : directDependencies) {
                CwatchArtifact cwa = new CwatchArtifact().GenCwatchArtifact(a);
                directArts.add(cwa);
            }
        }

        if (recursiveDependencies != null) {
            for (Artifact a : recursiveDependencies) {
                CwatchArtifact cwa = new CwatchArtifact().GenCwatchArtifact(a);
                recursiveArts.add(cwa);
            }
        }

    }


    public List<CwatchDependency> getPomDeps() {
        return pomDeps;
    }

    public List<CwatchArtifact> getAllArts() {
        return allArts;
    }

    public List<CwatchArtifact> getDirectArts() {
        return directArts;
    }

    public List<CwatchArtifact> getRecursiveArts() {
        return recursiveArts;
    }
}
