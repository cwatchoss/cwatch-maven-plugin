/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.transport;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import io.cwatch.utils.StatusCode;

import java.net.URL;

/**
 * CwatchMoveBus takes a CwatchBus structure and moves it to the cwatch API server via REST.
 * <p>
 * There is a utility function called "ping" to test the connection to the cWatch API server.
 *
 * @author Thorsten Zenker
 * @see CwatchBus
 */


public class CwatchMoveBus {
    static final String Kong_Auth_Error = "Invalid authentication credentials";
    static final String Kong_Override_Host = "X-Host-Override";

    String cwatch_key, cwatch_header, cwatch_url;
    String cwatch_host;


    /**
     * Creates a CwatchMoveBus interface
     *
     * @param key    cWatch API key: string with API token
     * @param header cWatch header name, standard is cwatch_key
     * @param url    cWatch URL of API server - variable for cWatch Enterprise edition
     * @see CwatchMoveBus
     */
    public CwatchMoveBus(String key, String header, String url) {

        this.cwatch_key = cleanProp(key);


        this.cwatch_header = cleanProp(header);
        this.cwatch_url = cleanProp(url);
        this.cwatch_host = "";
        try {
            URL aURL = new URL(this.cwatch_url);
            cwatch_host = aURL.getHost();
        } catch (Exception e) {
            // nothing to do - not a correct url, no host ...
        }

    }


    private String cleanProp(String prop) {
        if (prop == null) {
            return null;
        }
        return prop.trim().replaceAll("\t", "").replaceAll("\n", "").replaceAll(" ", "");
    }


    /**
     * Transfer data to cWatch server.
     *
     * @param cwatchBus bus data to transfer
     * @return JSon status string
     */
    public String MoveTheCwatchBus(CwatchBus cwatchBus) {

        String sres = "";
        try {
             /* Gson standard produces { "field":...} ... neded is: {"object":{"field"...}}     */
            Gson gson = new Gson();
            JsonElement je = gson.toJsonTree(cwatchBus);
            JsonObject jo = new JsonObject();
            jo.add("cwatchbus", je);

            Unirest.setTimeouts(3 * 60 * 1000, 2 * 60 * 1000);

            HttpResponse<JsonNode> jsonResponse = Unirest.post(cwatch_url + "/cwatchfeed")
                    .header("accept", "application/json")
                    .header("Content-Type", "application/json")
                    .header("accept-encoding", "gzip")
                    .header(cwatch_header, cwatch_key)
                    .header(Kong_Override_Host, cwatch_host)     // kong gets confused on host:port, send host without port
                    .body(jo.toString())
                    .asJson();

            if (jsonResponse.getStatus() == 200 && !jsonResponse.getBody().toString().contains(Kong_Auth_Error)) {
                StatusCode statusCode = new StatusCode();
                statusCode.setStatus("OK");
                statusCode.setError("");
                statusCode.setMessage("data for analysis teleported to cWatch server.");

                return statusCode.jsonStatusCode();
            }

            StatusCode statusCode = new StatusCode();
            statusCode.setStatus("Error");
            statusCode.setError("Communication problem: code: " + jsonResponse.getStatus() + ", error text: " + jsonResponse.getStatusText());
            statusCode.setMessage("Error while communicating with server. Contact cWatch support.");
            return statusCode.jsonStatusCode();

        } catch (UnirestException e) {

            StatusCode statusCode = new StatusCode();
            statusCode.setStatus("Error");
            statusCode.setError("communication response:" + e.getMessage());
            statusCode.setMessage("Error while communicating with server. Contact cWatch support.");
            sres = statusCode.jsonStatusCode();
        }

        return sres;
    }


    /**
     * ping cWatch server.
     *
     * @return JSon status string
     */
    public String ping() {

        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get(cwatch_url + "/ping").header(cwatch_header, cwatch_key).
                    header(Kong_Override_Host, cwatch_host).header("accept", "application/json").asJson();

            if (jsonResponse != null) {

                String sres = jsonResponse.getBody().toString();

                // special case: server returns valid json if "someone" pokes around the server without valid key ..
                if (sres != null && sres.contains(Kong_Auth_Error)) {
                    StatusCode statusCode = new StatusCode();
                    statusCode.setStatus("Error");
                    statusCode.setError("cWatch server returns on our contact: " + sres);
                    statusCode.setMessage("Please contact cWatch support.");

                    sres = statusCode.jsonStatusCode();
                }

                return sres;

            } else {
                // null data return?

                StatusCode statusCode = new StatusCode();
                statusCode.setStatus("Error");
                statusCode.setError("communication failed, received no data.");
                statusCode.setMessage("Please contact cWatch support.");

                return statusCode.toString();
            }

        } catch (UnirestException e) {
            StatusCode statusCode = new StatusCode();
            statusCode.setStatus("Error");
            statusCode.setError("communication attempt returned: " + e.getMessage());
            statusCode.setMessage("Please contact cWatch support.");

            return statusCode.jsonStatusCode();
        }
    }
}

