/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */


package io.cwatch.transport;

import javax.xml.bind.annotation.XmlRootElement;


/**
 * Defines a transport data structure: the CwatchBus.
 * The data structure is used to transfer data from this local
 * application to the cwatch.io API server.
 * <p>
 * Main components are:
 * driver: name of the driver
 * version: this is the version of the driver
 * content: the data to be transported aka th passenger.
 * <p>
 * The content structure "the passenger" must match with the driver and version.
 */

@XmlRootElement(name = "cwatchbus")
public class CwatchBus {

    String driver;              // driver is: "maven with optional bower guest"
    String version;             // version string. needed together with driver to interpret content
    String content;             // json content  (List of CwatchBusPassengerThe concept for


    public CwatchBus() {
        this.driver = "maven-bower";
        this.version = "1.0";
    }


    public void setContent(String content) {
        this.content = content;
    }


    public String getContent() {
        return content;
    }

    public String getDriver() {
        return driver;
    }

    public void setDriver(String driver) {
        this.driver = driver;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String version) {
        this.version = version;
    }
}
