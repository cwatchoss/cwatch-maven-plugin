/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.utils;

/**
 * String constants needed by various parts - mainly config and property reading.
 *
 * @author Thorsten Zenker
 */
public final class CwatchStaticContent {

    public static final String cWatch_Hello = "cWatch maven plugin, version 1.0.2";

    public static final String cWatch_key = "CWATCH_KEY";
    public static final String cWatch_key_default = "** NO VALID KEY **";

    public static final String cWatch_header = "CWATCH_HEADER";
    public static final String cWatch_header_default = "cwatch-key";


    public static final String cWatch_url = "CWATCH_URL";
    public static final String cWatch_url_default = "https://api.cwatch.io:443/cwatchuser/api/analysis";

    public static final String cWatch_commit_url = "CWATCH_COMMIT_URL";
    public static final String cWatch_commit_url_default = "https://api.cwatch.io:443/cwatchuser/api/commitbus";


    public static final String cWatch_debug = "CWATCH_VERBOSE";
    public static final String cWatch_debug_default = "false";

    public static final String cWatch_dump = "CWATCH_LOCALSTAT";
    public static final String cWatch_dump_default = "true";


}
