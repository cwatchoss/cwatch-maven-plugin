/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.utils;

import io.cwatch.mvn.CwatchWagonMojo;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

/**
 * Read properties from configuration file.
 *
 * @author Thorsten Zenker
 * @see CwatchWagonMojo
 */

public class ReadProperties {
    Properties prop;
    boolean propsLoaded = false;


    public void setPropsLoaded(boolean propsLoaded) {
        this.propsLoaded = propsLoaded;
    }

    /**
     * Determine value of properties config
     *
     * @param propertyKey bus data to transfer
     * @return value of property form cwatch.properties file or default value
     */

    public String readCwatchProperties(String propertyKey) {

        if (!propsLoaded) {
            loadInitial("cwatch.properties");
        }

        String sprops = prop.getProperty(propertyKey);


        if (sprops == null || sprops.length() == 0) {
            // no key value - go for default settings.

            if (propertyKey.contentEquals(CwatchStaticContent.cWatch_url)) {
                sprops = CwatchStaticContent.cWatch_url_default;
            }

            if (propertyKey.contentEquals(CwatchStaticContent.cWatch_header)) {
                sprops = CwatchStaticContent.cWatch_header_default;
            }

            if (propertyKey.contentEquals(CwatchStaticContent.cWatch_key)) {
                sprops = CwatchStaticContent.cWatch_key_default;
            }


            if (propertyKey.contentEquals(CwatchStaticContent.cWatch_debug)) {
                sprops = CwatchStaticContent.cWatch_debug_default;
            }

            if (propertyKey.contentEquals(CwatchStaticContent.cWatch_dump)) {
                sprops = CwatchStaticContent.cWatch_dump_default;
            }


            if (propertyKey.contentEquals(CwatchStaticContent.cWatch_commit_url)) {
                sprops = CwatchStaticContent.cWatch_commit_url_default;
            }


        }

        if (sprops != null) {
            sprops = sprops.trim().replaceAll("\t", "").replaceAll("\n", "").replaceAll(" ", "");
        }

        return sprops;
    }


    /**
     * Determine value of properties config.
     * Variable propsLoaded set to true on success.
     *
     * @param propName file name of properties file. Default: cwatch.properties
     */


    public void loadInitial(String propName) {

        if (propName == null || propName.length() == 0) {

            return;
        }

        prop = new Properties();
        InputStream input = null;

        try {
            input = new FileInputStream(propName);
            prop.load(input);
            propsLoaded = true;

        } catch (IOException ex) {

            // handled via boolean value
            // ex.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    // ignore this warning / notice
                    // e.printStackTrace();
                }
            }
        }
    }


}
