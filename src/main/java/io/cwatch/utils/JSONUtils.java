/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.utils;


import com.google.gson.Gson;

/**
 * Helper Class to find out if a string is a json string.
 */

public final class JSONUtils {

    private static final Gson gson = new Gson();

    private JSONUtils() {
    }

    public static boolean isJSONValid(String JSON_STRING) {

        if (JSON_STRING == null) {
            return false;
        }

        if (JSON_STRING.length() == 0) {
            return false;
        }

        try {
            gson.fromJson(JSON_STRING, Object.class);
            return true;
        } catch (com.google.gson.JsonSyntaxException ex) {
            return false;
        }
    }

}
