/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.mvn;

import io.cwatch.utils.CwatchStaticContent;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Component;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.RepositorySystemSession;
import org.eclipse.aether.repository.RemoteRepository;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.List;


/**
 * Mega Mojo structure used by the other maven targets
 *
 * @author Thorsten Zenker
 */


public class CwatchMegaMojo extends AbstractMojo {

    protected static final String cwatchPropertiesFile = "cwatch.properties";
    protected static final String cwatchVersion = "cwatch-maven-plugin:maven-plugin:1.0.1-SNAPSHOT";

    @Component
    protected RepositorySystem system;

    @Parameter(defaultValue = "${project}")
    protected MavenProject project;

    @Parameter(defaultValue = "${repositorySystemSession}")
    protected RepositorySystemSession repositorySystemSession;

    @Parameter(defaultValue = "${project.remoteProjectRepositories}")
    protected List<RemoteRepository> remoteRepositoryList;

    @Parameter(defaultValue = "${basedir}", property = "basedir", required = true)
    protected File projectDirectory;

    @Parameter(defaultValue = "${project.build.directory}", property = "outputDir", required = true)
    protected File outputDirectory;

    @Parameter(defaultValue = "${user.home}")
    protected File homeDirectory;


    /*
        optional path to cWatch Config File
     */

    @Parameter(property = "cwatchconfig", required = false)
    protected String cwatchConfig;


    @Parameter(defaultValue = "false", property = "verbose")
    protected String debug;

    @Parameter(defaultValue = "true", property = "dump", required = true)
    protected String dump;

    /*
        default values for communication with cWatch
    */
    String cwatch_key = CwatchStaticContent.cWatch_key_default;
    String cwatch_header = CwatchStaticContent.cWatch_header_default;
    String cwatch_url = CwatchStaticContent.cWatch_url_default;

    boolean booleanDebug = false;
    boolean booleanDump = false;

    /**
     * Default Mojo: see at implementation Mojos.
     */
    public void execute() throws MojoExecutionException, MojoFailureException {


    }


    /**
     * purpose is to find the configuration file.
     * looks to find properties file cwatch.properties in this order:
     * <p>
     * command line argument
     * project directory
     * /src/main/resource
     * <p>
     * * @return      path with file name to existing properties file or "null"
     */

    String findCwatchPropertiesPath() {
        String hereIsThePropertiesFile;

        final String cw_msg_read_from = "cwatch.io - reading config from: ";
        final String cw_msg_not_found = "cwatch.io - no config file found.";

        /*
            command line parameter overrules everything
        */
        if (cwatchConfig != null && cwatchConfig.length() > 0) {


            File file = new File(cwatchConfig);
            if (file.exists()) {


                getLog().info(cw_msg_read_from + file.getAbsolutePath());

                return cwatchConfig;
            }
        }

        /*
        local config overrules config within java
         */

        hereIsThePropertiesFile = projectDirectory + "/" + cwatchPropertiesFile;
        File file = new File(hereIsThePropertiesFile);
        if (file.exists()) {
            getLog().info(cw_msg_read_from + file.getAbsolutePath());
            return hereIsThePropertiesFile;
        }

        /*
        best way: put properties in resource directory
         */
        hereIsThePropertiesFile = projectDirectory + "/src/main/resource/" + cwatchPropertiesFile;
        file = new File(hereIsThePropertiesFile);
        if (file.exists()) {
            getLog().info(cw_msg_read_from + file.getAbsolutePath());
            return hereIsThePropertiesFile;
        }

        getLog().info(cw_msg_not_found);
        return null;
    }

    /**
     * purpose is to find the bower.json file.
     * <p>
     * looks to find bowers.json in this order:
     * <p>
     * TODO: search via parameter from command line
     * <p>
     * project directory
     *
     * @return path with file name to existing bower.json file or "null"
     */

    String findBowerJsonPath() {
        String hereIsThePropertiesFile;

        hereIsThePropertiesFile = projectDirectory + "/" + "bower.json";
        File file = new File(hereIsThePropertiesFile);
        if (file != null && file.exists()) {
            if (booleanDebug) {
                getLog().info("bower is at " + hereIsThePropertiesFile);
            }
            return hereIsThePropertiesFile;
        }


        return null;
    }


    /**
     * purpose is to read a file and return content as string.
     *
     * @param filePath String to file to be read into string.
     * @return string with file content ot IOEception
     * @throws IOException if file at filePath not readable
     */

    public String readFileAsString(String filePath) throws IOException {
        StringBuffer fileData = new StringBuffer();
        BufferedReader reader = new BufferedReader(
                new FileReader(filePath));
        char[] buf = new char[1024];
        int numRead = 0;
        while ((numRead = reader.read(buf)) != -1) {
            String readData = String.valueOf(buf, 0, numRead);
            fileData.append(readData);
        }
        reader.close();
        return fileData.toString();
    }


}
