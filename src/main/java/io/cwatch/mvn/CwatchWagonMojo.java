/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.mvn;


import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import io.cwatch.transport.CwatchBus;
import io.cwatch.transport.CwatchBusContent;
import io.cwatch.transport.CwatchBusPassenger;
import io.cwatch.transport.CwatchMoveBus;
import io.cwatch.utils.CwatchStaticContent;
import io.cwatch.utils.JSONUtils;
import io.cwatch.utils.ReadProperties;
import io.cwatch.utils.StatusCode;
import org.apache.commons.codec.binary.Base64;
import org.apache.maven.model.Dependency;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.collection.CollectRequest;
import org.eclipse.aether.graph.DependencyNode;
import org.eclipse.aether.resolution.DependencyRequest;
import org.eclipse.aether.util.graph.visitor.PreorderNodeListGenerator;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * CwatchWagonMojo is the main funtion to collect maven and bower data,
 * to filter it and to send it to the cWatch API server to analyze it.
 *
 * @author Thorsten Zenker
 */


@Mojo(name = "check", requiresDependencyResolution = ResolutionScope.TEST,
        defaultPhase = LifecyclePhase.PROCESS_SOURCES, threadSafe = false)
public class CwatchWagonMojo
        extends CwatchMegaMojo {

    @Parameter(property = "project.build.directory")
    private File outputDirectory;


    private List<Artifact> dependencies = new ArrayList<Artifact>();
    private List<Artifact> directDependencies = new ArrayList<Artifact>();
    private List<Artifact> recursiveDependencies = new ArrayList<Artifact>();

    private CwatchBus cwatchBus = new CwatchBus();
    private CwatchBusContent cwatchBusContent = new CwatchBusContent();

    private String rootArtifact = "";

    /**
     * Main entry into mojo.
     *
     * @throws MojoExecutionException with a message if an error occurs.
     */
    public void execute()
            throws MojoExecutionException {


        boolean haveProperties = true;


        String cwatchPropertyFileA = findCwatchPropertiesPath();
        if (cwatchPropertyFileA == null) {
            getLog().info("------ no property file found!");
            haveProperties = false;
        }
        ReadProperties readProperties = new ReadProperties();
        readProperties.loadInitial(cwatchPropertyFileA);

        cwatch_key = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_key);
        cwatch_header = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_header);
        cwatch_url = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_url);


        // determine verbose settings
        booleanDebug = false;
        booleanDebug = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_debug).contentEquals("true");
        // command line over rules cwatch.properties settings ...
        if (debug != null && debug.trim().contentEquals("true")) {
            booleanDebug = true;
        }


        // determine verbose settings
        booleanDump = true;
        booleanDump = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_dump).trim().contentEquals("true");
        /* command line overwrites config file .. */
        if (dump != null) {
            booleanDump = dump.trim().contentEquals("true");
        }


        if (booleanDebug) {
            getLog().info("------ header:");
            getLog().info(CwatchStaticContent.cWatch_Hello);
            getLog().info("CWATCH_KEY                  : " + cwatch_key);
            getLog().info("CWATCH_HEADER               : " + cwatch_header);
            getLog().info("CWATCH_URL                  : " + cwatch_url);
            getLog().info("CWATCH_LOCALSTAT            : " + booleanDump);
            getLog().info("CWATCH_VERBOSE              : " + booleanDebug);
        }

        List<Dependency> thisProjectDependencies = project.getDependencies();

        gatherDependencies();


        if (booleanDebug) {
            getLog().info("------ maven stats:");
            getLog().info("group:artifact:version      : " + rootArtifact);
            getLog().info("cWatch size dependency      : " + dependencies.size());
            getLog().info("cWatch size direct          : " + directDependencies.size());
            getLog().info("cWatch size recursive       : " + recursiveDependencies.size());
        }


        cwatchBusContent.loadBus(thisProjectDependencies, dependencies, directDependencies, recursiveDependencies);

        if (booleanDebug) {
            getLog().info("------ transport size:");
            getLog().info("cWatch Bus size: pom        : " + cwatchBusContent.getPomDeps().size());
            getLog().info("cWatch Bus size: dependency : " + cwatchBusContent.getAllArts().size());
            getLog().info("cWatch Bus size: direct     : " + cwatchBusContent.getDirectArts().size());
            getLog().info("cWatch Bus size: recursive  : " + cwatchBusContent.getRecursiveArts().size());
        }


        List<CwatchBusPassenger> passengers = new ArrayList<CwatchBusPassenger>();

        // put passengers in the bus
        Gson gson = new Gson();
        String mavenBackpack = gson.toJson(cwatchBusContent);

        // setup maven passenger
        CwatchBusPassenger mavenPassenger = new CwatchBusPassenger();
        mavenPassenger.setName("maven-v331");
        byte[] encodedBytes = Base64.encodeBase64(mavenBackpack.getBytes());
        mavenPassenger.setBackpack(new String(encodedBytes));
        passengers.add(mavenPassenger);


        String bowerPath = findBowerJsonPath();
        if (bowerPath != null) {
            if (booleanDebug) {
                getLog().info("cWatch: found bower.json");
            }

            String bowerBackpack = null;
            try {
                bowerBackpack = readFileAsString(bowerPath);
                byte[] encodedBowerBytes = Base64.encodeBase64(bowerBackpack.getBytes());
                bowerBackpack = new String(encodedBowerBytes);

            } catch (IOException e) {
                getLog().error("cWatch: position C. Error " + e.getMessage());
            }
            if (bowerBackpack != null) {
                if (booleanDebug) {
                    getLog().info("cWatch bower.json base64. Length is: " + bowerBackpack.length());
                }
                CwatchBusPassenger bowerPassenger = new CwatchBusPassenger();
                bowerPassenger.setName("bower");
                bowerPassenger.setBackpack(bowerBackpack);
                passengers.add(bowerPassenger);
            }
        }

        // all passengers go into the Bus:
        String busContent = gson.toJson(passengers);
        cwatchBus.setContent(busContent);

        if (booleanDebug) {
            getLog().info("cWatch Bus content length   : " + cwatchBus.getContent().length());
        }

        // press the "red big" start button ;-)

        String sres = null;
        if (booleanDebug) {

            String callYesNo = "no";
            if (haveProperties) {
                callYesNo = "true";
            }

            getLog().info("cWatch call API server      : " + callYesNo);

        }


        if (haveProperties) {
            try {
                if (booleanDebug) {
                    getLog().info("cWatch: driver              : " + cwatchBus.getDriver() + ", version: " + cwatchBus.getVersion());
                }

                CwatchMoveBus cwatchMoveBus = new CwatchMoveBus(cwatch_key, cwatch_header, cwatch_url);
                sres = cwatchMoveBus.MoveTheCwatchBus(cwatchBus);


            } catch (Exception e) {
                getLog().info("cWatch: position D. Error " + e.getMessage());
            }

            if (!JSONUtils.isJSONValid(sres)) {
                StatusCode statusCode = new StatusCode();

                statusCode.setStatus("Error");
                statusCode.setError("error detail: " + sres + ", in contact with server: " + cwatch_url);
                statusCode.setMessage("Please contact cWatch support.");

                getLog().error(statusCode.jsonStatusCode());

            }

        }


        // standard is to place these files in the target/ directory
        if (booleanDump) {

            Gson gsonOut = new GsonBuilder().serializeNulls().setPrettyPrinting().create();

            dumpLocal(gsonOut.toJson(cwatchBusContent.getPomDeps()), "cwatch-pom.json");
            dumpLocal(gsonOut.toJson(cwatchBusContent.getAllArts()), "cwatch-all.json");
            dumpLocal(gsonOut.toJson(cwatchBusContent.getDirectArts()), "cwatch-direct.json");
            dumpLocal(gsonOut.toJson(cwatchBusContent.getRecursiveArts()), "cwatch-recursive.json");

        }

        // force reload - valuable for sub-projects
        readProperties.setPropsLoaded(false);


        getLog().info(sres);

    }


    /**
     * write content from fileContent into file outFilename in directory outputDirectory.
     *
     * @param fileContent String with content to be writte to a file
     * @param outFileName name of file within outputDirectory
     * @throws MojoExecutionException
     */


    private void dumpLocal(String fileContent, String outFileName) throws MojoExecutionException {

        File directory = outputDirectory;

        if (!directory.exists()) {
            directory.mkdirs();
        }

        File file = new File(directory, outFileName);
        FileWriter fileWriter = null;
        try {
            fileWriter = new FileWriter(file);
            fileWriter.write(fileContent);
        } catch (IOException e) {
            throw new MojoExecutionException("cWatch: Error creating file " + file, e);
        } finally {
            if (fileWriter != null) {
                try {
                    fileWriter.close();
                } catch (IOException e) {
                    // ignoring this case is OK.
                }
            }
        }
    }


    private void gatherDependencies() {

        try {

            Artifact artifact = new DefaultArtifact(project.getArtifact().toString());

            rootArtifact = artifact.getGroupId() + ":" + artifact.getArtifactId() + ":" + artifact.getVersion();

            DefaultArtifact defaultArtifact = new DefaultArtifact(artifact.getGroupId(), artifact.getArtifactId(), "pom", artifact.getVersion());
            CollectRequest collectRequest = new CollectRequest();
            collectRequest.setRoot(new org.eclipse.aether.graph.Dependency(defaultArtifact, "compile"));
            collectRequest.setRepositories(remoteRepositoryList);


            DependencyNode root = system.collectDependencies(repositorySystemSession, collectRequest).getRoot();
            DependencyRequest dependencyRequest = new DependencyRequest(root, null);
            system.resolveDependencies(repositorySystemSession, dependencyRequest);
            PreorderNodeListGenerator nodeListGenerator = new PreorderNodeListGenerator();
            root.accept(nodeListGenerator);


            for (org.eclipse.aether.graph.Dependency dependency : nodeListGenerator.getDependencies(true)) {
                dependencies.add(dependency.getArtifact());
            }

            for (DependencyNode dependencyNode : root.getChildren()) {
                directDependencies.add(dependencyNode.getDependency().getArtifact());
            }

            recursiveDependencies = new ArrayList<Artifact>(dependencies);
            recursiveDependencies.removeAll(directDependencies);

        } catch (Exception e) {
            getLog().error("cWatch: position B. Error " + e.getMessage());
        }

    }

}