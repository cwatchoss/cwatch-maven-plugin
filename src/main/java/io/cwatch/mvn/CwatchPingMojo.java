/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.mvn;

import io.cwatch.transport.CwatchMoveBus;
import io.cwatch.utils.CwatchStaticContent;
import io.cwatch.utils.JSONUtils;
import io.cwatch.utils.ReadProperties;
import io.cwatch.utils.StatusCode;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Send a "ping" via network to the cWatch server.
 * Expected result is: {"status":"OK","message":"PONG","error":""}
 */

@Mojo(name = "ping",
        defaultPhase = LifecyclePhase.VALIDATE, threadSafe = true)
public class CwatchPingMojo extends CwatchMegaMojo {

    /**
     * Main entry into ping mojo.
     *
     * @throws MojoExecutionException with a message if an error occurs.
     */
    public void execute()
            throws MojoExecutionException {

        String cwatchPropertyFile = findCwatchPropertiesPath();
        ReadProperties readProperties = new ReadProperties();
        readProperties.loadInitial(cwatchPropertyFile);


        cwatch_key = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_key);
        cwatch_header = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_header);
        cwatch_url = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_url);


        CwatchMoveBus cwatchMoveBus = new CwatchMoveBus(cwatch_key, cwatch_header, cwatch_url);
        String sres = cwatchMoveBus.ping();


        StatusCode statusCode = new StatusCode();

        if (!JSONUtils.isJSONValid(sres)) {
            statusCode.setStatus("Error");
            statusCode.setError("Error returned was: " + sres + ", server called was: " + cwatch_url);
            statusCode.setMessage("Please contact cWatch support.");

            sres = statusCode.jsonStatusCode();
        }


        booleanDebug = false;

        /* command line overwrites config file .. */
        if (dump != null) {
            booleanDump = dump.contentEquals("true");
        } else {
            booleanDebug = readProperties.readCwatchProperties(CwatchStaticContent.cWatch_dump).contentEquals("true");
        }


        if (booleanDebug) {
            getLog().info("Contacting cWatch server at: " + cwatch_url + "/ping");
        }
        getLog().info(sres);

    }

}