/*
 * Copyright 2015 TZ Consulting GmbH, Thorsten Zenker
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package io.cwatch.mvn;

import io.cwatch.utils.CwatchStaticContent;
import io.cwatch.utils.ReadProperties;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;

/**
 * Display "help" information for the cWatch maven plugin.
 *
 * @author Thorsten Zenker
 */


@Mojo(name = "help",
        defaultPhase = LifecyclePhase.VALIDATE, threadSafe = true)
public class CwatchHelpMojo extends CwatchMegaMojo {


    /**
     * Main entry into ping mojo.
     *
     * @throws MojoExecutionException with a message if an error occurs.
     */
    public void execute()
            throws MojoExecutionException {


        // determine verbose settings
        booleanDebug = true;

        ReadProperties readProperties = new ReadProperties();
        String cwatchPropertyFilePath = findCwatchPropertiesPath();
        readProperties.loadInitial(cwatchPropertyFilePath);


        getLog().info("Welcome to the cWatch maven plugin: " + CwatchStaticContent.cWatch_Hello);
        getLog().info("");
        getLog().info("usage: mvn io.cwatch:cwatch-maven-plugin:1.0.2:[goal]");
        getLog().info("");
        getLog().info("Goals are:");
        getLog().info("ping   : Test connection to cWatch server.");
        getLog().info("         mvn io.cwatch:cwatch-maven-plugin:1.0.2:ping");
        getLog().info("check  : Start cWatch analysis.");
        getLog().info("         mvn io.cwatch:cwatch-maven-plugin:1.0.2:check");
        getLog().info("help   : Display help.");
        getLog().info("         mvn io.cwatch:cwatch-maven-plugin:1.0.2:help");
        getLog().info("");
        getLog().info("Reach out via https://www.cwatch.io - questions & help: we are here to support you.");
        getLog().info("");
        getLog().info("cWatch version of this plugin: " + cwatchVersion);
        getLog().info("");
    }

}
